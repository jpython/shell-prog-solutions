#!/usr/bin/awk -f

BEGIN { total = 0; }

! /^ *#/ {
	total += $2 * $3;
}

END { printf("Total : %.2f\n", total); }
