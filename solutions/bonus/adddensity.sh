#!/usr/bin/env bash

csv() {
    local items=("$@")
    
    ( IFS=','
      echo -n "${items[*]}" )
}

dos2unix | sed -e 's/\"//g' | \
while IFS=',' read city pop area gent
do
    dens=$( echo "scale=2; $pop / $area" | bc ) 
    echo -n \"
    csv $city $pop $area $gent $dens
    echo \"
done | sed -e 's/,/\",\"/g'
